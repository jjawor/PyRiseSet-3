import argparse
import os
from pyriseset3 import sites, utils, sourcelist, actions
from pyriseset3.edd_checker import main as format_check


def write_header(file, header_name):
    """
    Write a header to the schedule file.

    Parameters:
        file (file): The schedule file to write to.
        header_name (str): The name of the header.
    """

    # Define the headers
    headers = {'UBB': 'FE:P170mm ; EDD_PULSAR_EPTA ; Init 1;',
               'P217': 'FE:P217mm ; EDD_PULSAR_EPTA',
               'S110': 'FE:S110mm ; EDD_PULSAR',
               'S60': 'FE:S60mm ; EDD_PULSAR_MULTIFIBA'}
    # Write the header to the file
    file.write(headers[header_name] + '\n\n')


def write_source(file, source_name, ra_hm, dec_hm, time, lat_off=0.0,
                 lon_off=0.0, mode='obs', suffix=''):
    """
    Write a source to the schedule file.

    Parameters:
        file (file): The schedule file to write to.
        source_name (str): The name of the source.
        ra_hm (str): The right ascension of the source in hours and minutes.
        dec_hm (str): The declination of the source in degrees and minutes.
        time (int): The time to observe the source in seconds.
        lat_off (float, optional): The latitude offset. Default is 0.0.
        lon_off (float, optional): The longitude offset. Default is 0.0.
        mode (str, optional): The observing mode. Default is 'obs'.
        suffix (str, optional): A suffix to append to the source name. Default is ''.
    """

    # Define the observing modes
    modestr = {'obs': 'Search', 'cal': 'CalFE'}

    # Format the string to write to the file
    string = ('{}{} ; CoordinateSystem Equatorial ; Equinox J2000 ; '
              'ObjectLongitude {} ; ObjectLatitude {} ; LatOff {} ; LonOff {} ; '
              'PMODE {} ; SCANTime {}\n')\
                .format(source_name, suffix, ra_hm, dec_hm, lat_off, lon_off,
                        modestr[mode], time)

    # Write the string to the file
    file.write(string)


def EDD_opt(file, percentage=0.0):
    """
    Write the EDD options to the schedule file.

    Parameters:
        file (file): The schedule file to write to.
        percentage (float, optional): The percentage of time to use the noise diode. Default is 0.0.
    """

    # Format the string to write to the file
    string = ('SENDTO EDD JSON MEASUREMENTPREPARE {{"dig_pack_controller_2G":'
              '{{"noise_diode_pattern":{{"percentage":"{}","period":"1.0"}}}}}}\n').format(percentage) 

    # Write the string to the file
    file.write(string)


def main():
    """Create an Effelsberg format schedule from a pyriseset3 schedule"""

    srclist = args.sources

    # Create the output schedule file
    schedule_path = os.path.join(args.outpath, args.outname)
    schedulesch = open(schedule_path, 'w')

    if args.header is not None:
        # Write the header
        write_header(schedulesch, args.header)

    for source in srclist:
        # Write the source to the schedule file
        ra = source.ra_deg
        de = source.decl_deg
        cal_time_sec = 120
        # source_time is to be the total time on source in seconds (without cal time!)
        source_time = int(source.notes)*60 - cal_time_sec

        # Convert the coordinates to hours and minutes
        ra_hm = str(utils.deg_to_hmsstr(ra, 2, style='units')[0])
        dec_hm = str(utils.deg_to_dmsstr(de, 2, style='units',
                                         explicit_sign="True")[0])

        if source.name.startswith('3C'):
            # Polcal source
            if args.UBB:
                EDD_opt(schedulesch, percentage=0.5)
            write_source(schedulesch, source.name, ra_hm, dec_hm, cal_time_sec,
                         lat_off=1.0, mode='cal', suffix='_N_R')
            if args.UBB:
                EDD_opt(schedulesch, percentage=0.5)
            write_source(schedulesch, source.name, ra_hm, dec_hm, cal_time_sec,
                         lat_off=0.0, mode='cal', suffix='_O_R')
            if args.UBB:
                EDD_opt(schedulesch, percentage=0.5)   
            write_source(schedulesch, source.name, ra_hm, dec_hm, cal_time_sec,
                         lat_off=-1.0, mode='cal', suffix='_S_R')         

        else:
            # Normal source
            if args.UBB:
                EDD_opt(schedulesch, percentage=0.5)
            write_source(schedulesch, source.name, ra_hm, dec_hm, cal_time_sec,
                         lat_off=0.5, mode='cal', suffix='_R')
            if args.UBB:
                EDD_opt(schedulesch, percentage=0.0)
            write_source(schedulesch, source.name, ra_hm, dec_hm, source_time)

        schedulesch.write('\n\n')

    schedulesch.close()

    # check the schedule file for inconsistencies
    format_check(schedule_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Generate an Efflesberg format"
                                     " schedule file from a pyriseset3 format schedule.")
    parser.set_defaults(sources=sourcelist.SourceList(name="sources"))
    parser.add_argument('-p', '--target', type=str, \
                        action=actions.AppendSourceCoords, dest='sources', \
                        help="A string describing a target. Format should "
                            "be '<name> <ra> <decl> [<notes>]'. "
                            "Be sure to quote the string.")
    parser.add_argument('--target-file',type=str,
                        action=actions.ExtendSourceCoords, dest='sources',
                        help="Read targets from files. Format should "
                            "be '<name> <ra> <decl> [<notes>]'.")
    parser.add_argument('--start-utc', dest='start_utc',
                        action=actions.ParseTime, default=0,
                        help="Universal Time to start at. Can be given "
                            "as a string (in HH:MM:SS.SS format). Or as "
                            "a floating point number (in hours). (Default: 0)")
    parser.add_argument('--end-utc', dest='end_utc', \
                        action=actions.ParseTime, default=24,
                        help="Universal Time to end at. Can be given "
                            "as a string (in HH:MM:SS.SS format). Or as "
                            "a floating point number (in hours). (Default: 24)")
    parser.add_argument('--date', type=str, default=None,
                        action=actions.ParseDate,
                        help="Date to use (in YYYY-MM-DD format). " 
                            "(Default: today)")
    parser.add_argument('--list-sites', action=actions.ListSitesAction,
                        nargs=0,
                        help="List registered observing sites, and exit.")
    parser.add_argument('--site', dest='site', type=str,
                        default=sites.registered_sites[0],
                        help="Name of observing site to load. Use "
                            "'--list-sites' to get a list of accepted "
                            f"sites. (Default: '{sites.registered_sites[0]}')")
    parser.add_argument('--UBB', dest='UBB', action='store_true',
                        help="Use UBB style formatting (extra EDD options)")
    parser.add_argument('--header', dest='header',
                        choices=['UBB', 'P217', 'S110', 'S60'], default=None,
                        help="Create a header corresponding to a chosen "
                        "reciever. Valid options are: UBB, P217, S110, S60."
                        "Defaults to None (don't write a header).")
    parser.add_argument('-o', '--outpath', dest='outpath', type=str,
                        default=os.getcwd(),
                        help='Output directory for the schedule file. Defaults '
                        'to the current working directory')
    parser.add_argument('-n', '--outname', dest='outname', type=str,
                        default='schedule.sch',
                        help='Name of the new schedule file. Defaults to '
                        '"schedule.sch"')
    args = parser.parse_args()
    main()

