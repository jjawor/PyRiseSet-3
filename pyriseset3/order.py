#!/usr/bin/env python

import sys
import argparse
import datetime
from math import ceil
from math import floor

from string import replace

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from pyriseset import sites
from pyriseset import errors
from pyriseset import utils
from pyriseset import sources
from pyriseset import sourcelist
from pyriseset import actions

def rise_set(source, site, date):
    ra = source.ra_deg
    de = source.decl_deg
#       print utils.deg_to_hmsstr(ra)[0]
#-------Rise and set time of the pulsar
    try:
        rise_time, set_time = source.get_rise_set_times(site, date)
#        print rise_time, set_time
        rise_time_utc = site.lst_to_utc(rise_time, date)
        set_time_utc = site.lst_to_utc(set_time, date)
#        print rise_time_utc, set_time_utc
    except errors.SourceIsCircumpolar:
        rise_time_utc = args.start_utc
        set_time_utc = args.end_utc
    except errors.MultipleRiseSets:
        rise_time_utc = args.start_utc
        set_time_utc = args.end_utc

#----- Convert rise_time_utc in order to be comparable with start_utc_pulsar 
    if rise_time_utc < args.start_utc:
        rise_time_24 = rise_time_utc + 24.0
    else :
        rise_time_24 = rise_time_utc
    if set_time_utc < args.start_utc:
        set_time_24 = set_time_utc + 24.0
    else :
        set_time_24 = set_time_utc

    return rise_time_utc, set_time_utc

              
def main():
    site = sites.load(args.site)
    if args.date is None:
        date = datetime.date.today()
    else:
        date = args.date
    srclist = args.sources
    start_utc = args.start_utc
    start_utc_pulsar = start_utc

    alt_other = 0
    az_other = 0
    i = 0
    cal_time_sec = 120

    schedule = open('schedule.txt', 'w')
    schedule.write('PSR \t  TIME \t  Note    \n')
    
    for source in srclist:
#        print source, type(source)
        print source.name

	ra = source.ra_deg
	de = source.decl_deg
	
#-------Rise and set time of the pulsar
        try:
            rise_time, set_time = source.get_rise_set_times(site, date)
#        print rise_time, set_time
            rise_time_utc = site.lst_to_utc(rise_time, date)
            set_time_utc = site.lst_to_utc(set_time, date)
#	print rise_time_utc, set_time_utc
        except errors.SourceIsCircumpolar:
            rise_time_utc = args.start_utc
            set_time_utc = args.end_utc
        except errors.MultipleRiseSets:
            rise_time_utc = args.start_utc
            set_time_utc = args.end_utc

#----- Convert rise_time_utc in order to be comparable with start_utc_pulsar 
        if rise_time_utc < start_utc :
            rise_time_24 = rise_time_utc + 24.0
        else :
            rise_time_24 = rise_time_utc
        if set_time_utc < start_utc  :
            set_time_24 = set_time_utc + 24.0
        else :
            set_time_24 = set_time_utc
        print "Rise", rise_time_24, "Set", set_time_24

#------------ Integration time 
        int_time = float(source.notes) * 0.01666666666667
        print "integration time (hours)", int_time

#------------------------------------------------
#------------------------------------------------
#------ Add the slew time in the start_utc_pulsar starting from the second pulsar in the list
	if i !=0 :
		alt, az = source.get_altaz(site, start_lst_pulsar, date)
                slew_time = site.slew_time((alt, az), (alt_other, az_other)) 
		slew_time /= 60
#                print slew_time, ceil(slew_time)
                if slew_time > 5.0 :
                        print "W A R N I N G Change order it takes a lot of time to the telescope to slew "
                else:
                        print "It takes less than 5min for the telescope to point the source !!! "

                print "Slew time between this pulsar and the previous one, min, hours", slew_time, ceil(slew_time) * 0.0166666667
                start_utc_pulsar = start_utc_pulsar + ceil(slew_time) * 0.0166666667  + 0.00833333333333 # an extra half a minute

#------Convert the begining of each pulsar scan from utc to lst  
        print "The observation scan starts at (utc)", start_utc_pulsar
        start_lst_pulsar = site.utc_to_lst(start_utc_pulsar, date)
#        print "start in lst", start_lst_pulsar
        print "The observation ends (UTC)", start_utc_pulsar + int_time
#------------------------------
        if source.is_visible(site, start_lst_pulsar, date) == True :        
		if abs(start_utc_pulsar - rise_time_24) < 0.166667  : 
		    print "W A R N I N G Too close to the rise. In hours ", abs(start_utc_pulsar - rise_time_24), "\n"
        	elif  abs(set_time_24 - start_utc_pulsar - int_time)  < 0.166667:
	    	    print "W A R N I N G Too close to set. In hours", abs(set_time_24 - start_utc_pulsar - int_time), "\n"
        	else : 
            	    print "Fine !!! Hour distance from rise", abs(start_utc_pulsar - rise_time_24), "and set ", abs(set_time_24 - start_utc_pulsar - int_time), "\n"
        else:
            	print "F A L S E The pulsar in not visible \n"	

        if start_utc_pulsar >= 24.0:
       	    schedule.write(str(source.name) + '\t' + str(int(floor(start_utc_pulsar-24.0))) + ':' + str( int(ceil((start_utc_pulsar - 24.0 - floor(start_utc_pulsar - 24.0)) / 0.016666666667)) )  + ' - ' + str(int(floor(start_utc_pulsar -24.0 + int_time))) + ':' + str( int(ceil((start_utc_pulsar - 24.0 + int_time - floor(start_utc_pulsar - 24.0 + int_time)) / 0.016666666667)) ) + '\t' + '\n' )
        else:
            schedule.write(str(source.name) + '\t' + str(int(floor(start_utc_pulsar))) + ':' + str( int(ceil((start_utc_pulsar - floor(start_utc_pulsar)) / 0.016666666667)) )  + ' - ' + str(int(floor(start_utc_pulsar + int_time))) + ':' + str( int(ceil((start_utc_pulsar + int_time - floor(start_utc_pulsar + int_time)) / 0.016666666667)) ) + '\t' + '\n' )

#------- Reset the scan start           
        start_utc_pulsar = start_utc_pulsar + int_time
        start_lst_pulsar = site.utc_to_lst(start_utc_pulsar, date)
#------------------------------------
#-------- We measure the alt and a of old pulsar pulsar in order to have a measurement in the same time 
	alt_other, az_other = source.get_altaz(site, start_lst_pulsar, date)
#	print "Inside the Slew start ust", start_utc_pulsar, "alt_old, az_old", alt_other, az_other

	i = i + 1
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Plot sources time ranges " \
                        "when sources are above the horizon.")
    parser.set_defaults(sources=sourcelist.SourceList(name="sources"))
    parser.add_argument('-p', '--target', type=str, \
                        action=actions.AppendSourceCoords, dest='sources', \
                        help="A string describing a target. Format should " \
                            "be '<name> <ra> <decl> [<notes>]'. " \
                            "Be sure to quote the string.")
    parser.add_argument('--target-file',type=str, \
                        action=actions.ExtendSourceCoords, dest='sources', \
                        help="Read targets from files.")
    parser.add_argument('--start-utc', dest='start_utc', \
                        action=actions.ParseTime, default=0, \
                        help="Universal Time to start at. Can be given " \
                            "as a string (in HH:MM:SS.SS format). Or as " \
                            "a floating point number (in hours). (Default: 0)")
    parser.add_argument('--end-utc', dest='end_utc', \
                        action=actions.ParseTime, default=24, \
                        help="Universal Time to end at. Can be given " \
                            "as a string (in HH:MM:SS.SS format). Or as " \
                            "a floating point number (in hours). (Default: 24)")
    parser.add_argument('--date', type=str, default=None, \
                        action=actions.ParseDate, \
                        help="Date to use (in YYYY-MM-DD format). " \
                            "(Default: today)")
    parser.add_argument('--list-sites', action=actions.ListSitesAction, \
                        nargs=0, \
                        help="List registered observing sites, and exit.")
    parser.add_argument('--site', dest='site', type=str, \
                        default=sites.registered_sites[0], \
                        help="Name of observing site to load. Use " \
                            "'--list-sites' to get a list of accepted " \
                            "sites. (Default: '%s')" % \
                            sites.registered_sites[0])
    args = parser.parse_args()
    main()

