import json
import argparse
import time
def validate_json(json_string):
    try:
        json.loads(json_string)
        return True
    except ValueError:
        return False

def main(input_file,  output_file=None):
    prep_lines = []
    output_lines = []
    invalid_json_lines = []
    with open(input_file, 'r') as f:
        for i, line in enumerate(f, start=1):
            if line.startswith("FE:"):
                output_lines.append(line.strip())
                print(line.strip())
            elif line.startswith("SENDTO EDD JSON MEASUREMENTPREPARE"):
                if validate_json(line.split("MEASUREMENTPREPARE ")[-1].strip()):
                    prep_lines.append("SENDTO EDD JSON MEASUREMENTPREPARE "+line.split("MEASUREMENTPREPARE ")[-1].strip().replace(" ",""))
                else:
                    invalid_json_lines.append(i)
            elif line.startswith((";", "#", "\n")):
                output_lines.append(line.strip())
                print(line.strip())
            elif line.startswith((" ", "\t")):
                raise ValueError("Invalid line format: {}".format(line.strip()))
            else:
                if not prep_lines:
                    continue
                else:
                    current_prep_line = prep_lines[-1]
                output_lines.append(current_prep_line.strip())
                output_lines.append(line.strip())
                print(current_prep_line.strip())
                print(line.strip())

    if invalid_json_lines:
        print(f"\nThe following lines have invalid JSON: {', '.join(map(str, invalid_json_lines))}")
        print("JSON ERROR DECTECTED, NOT WRITING TO FILE")
        exit()

    if output_file is not None:
        with open(output_file, 'w') as f:
            f.write("\n".join(output_lines))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Validate telescope observing file.')
    parser.add_argument('input_file', type=str, help='path to input file')
    parser.add_argument('-o', '--output_file', type=str, help='path to output file')
    args = parser.parse_args()
    main(args.input_file, args.output_file)
