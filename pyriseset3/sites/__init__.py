import importlib
# import sites.effelsberg

registered_sites = ["effelsberg", \
                    "arecibo", \
                    "jodrell", \
                    "gbt", \
                    "lofar", \
                    "ubb", \
                   ]
__all__ = registered_sites
# from . import *
# 
site_aliases = {'eff':'effelsberg',
                'g':'effelsberg',
                'ao':'arecibo',
                '3':'arecibo',
                'jbo':'jodrell',
                '8':'jodrell',
                '1':'gbt'}


def load(sitename):
    sitename = site_aliases.get(sitename.lower(), sitename)
    if sitename in registered_sites:
        site = importlib.import_module('.'+sitename, __name__)
        # site = __import__(sitename, globals())
        return site.Site()
    else:
        raise ValueError("Site cannot be loaded. Name provided " \
                            "is not recognized: '%s'" % sitename)
